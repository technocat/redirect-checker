#!/bin/bash
# MIT license
# Madeline Holland

# Requires wget

#This script is designed to be used with the following set as environment variables:

#Set the protocol to http or https (required)
#PROTOCOL=http

#Set the site url, do not set https or use a trailing slash (required)
#SITE_URL=site.example.com

#Set the http basic auth username and password (required to set both or neither)
#UNAME=username
#PASS=password

#Set the output csv file
#OUTPUT=broken.csv

#Set the base delay, in seconds, which 1 to 3 seconds will be added onto
#DELAY=1

#Set if the output of the wget command should be logged
#CHECK=true

#Set the directory into which the output of the wget command should be logged
#ERROR_OUT=errors

iteration=1
declare -i iteration
AUTH=0
declare -i AUTH

function check_variables () {
  if [ -z ${SITE_URL+x} ]
  then
    echo "Need SITE_URL to be set to a base url without http(s)://"
    echo "For example SITE_URL=www.example.com"
    exit 1
  fi
  
  if [ -z ${UNAME+x} ]
  then
    if [ -z ${PASS+x} ]
    then
      echo "Need UNAME if PASS is set."
      exit 1
    fi
    echo "Username & password not set, continuing."
    AUTH=0
  else 
    if [ -z ${PASS+x} ] 
    then 
      echo "Need PASS if UNAME is set."
      exit 1
    else
      AUTH=1
    fi
  fi

  if [ -z ${PROTOCOL+x} ]
  then
    echo "PROTOCOL not set, defaulting to https"
    echo ""
    PROTOCOL=https
  fi

  if [ -z ${OUTPUT+x} ]
  then
    echo "OUTPUT not set, setting to not_working.csv"
    echo ""
    OUTPUT=not_working.csv
  fi

  if [ -z ${DELAY+x} ]
  then
    echo "DELAY not set, setting to 1 second"
    echo ""
    DELAY=1
  fi

  if [ -z ${CHECK+x} ]
  then
    echo "CHECK not set, setting to false"
    echo ""
    CHECK="false"
  fi

  if [ -z ${ERROR_OUT+x} ] && [ $CHECK == true ]
  then
    echo "ERROR_OUT not set, but CHECK is set, setting to dump errors logs into the errors directory"
    echo ""
    ERROR_OUT=errors
  fi

  if [ $CHECK == true ]
  then
    mkdir -p $ERROR_OUT
    c=$?
    if [ $c -ne 0 ];
    then
      echo "Cannot make $ERROR_OUT directory, trying to print to current directory"
      echo ""
      echo "test" > test-file.txt
      c=$?
      if [ $c -ne 0 ];
      then
        echo "Cannot write to current or $ERROR_OUT directories, quitting."
        exit 1
      else
        echo "Can write to current directory, writing error logs there."
        echo ""
        rm test-file.txt
        ERROR_OUT=$(pwd)
      fi
    fi
  fi
}

function check_noauth () {
  if [ $(wget --output-document=/dev/null $PROTOCOL://$SITE_URL${path} 2>&1 | grep -c $new_path) -ge 1 ];
  then
    echo "Working"
  else
    echo "Failed"
    echo "$PROTOCOL://$SITE_URL${path},$PROTOCOL://$SITE_URL${new_path},$iteration" >> $OUTPUT
    if [ $CHECK == "true" ];
    then
      echo ""
      echo "Trying again after pausing."
      sleep $((1 + RANDOM % DELAY))s
      echo "Writing output to ERROR_OUT/error-line-$iteration.log:"
      wget --output-document=/dev/null $PROTOCOL://$SITE_URL${path} 2>&1 | tee $ERROR_OUT/error-line-$iteration.log
    fi
  fi
}

function check_auth () {
  if [ $(wget --user=$UNAME --password=$PASS --output-document=/dev/null $PROTOCOL://$SITE_URL${path} 2>&1 | grep -c $new_path) -ge 1 ];
  then
    echo "Working"
  else
    echo "Failed"
    echo "$PROTOCOL://$SITE_URL${path},$PROTOCOL://$SITE_URL${new_path},$iteration" >> $OUTPUT
    if [ $CHECK == "true" ];
    then
      echo ""
      echo "Trying again after pausing."
      sleep $((1 + RANDOM % DELAY))s
      echo "Writing output to ERROR_OUT/error-line-$iteration.log:"
      wget --user=$UNAME --password=$PASSWORD --output-document=/dev/null $PROTOCOL://$SITE_URL${path} 2>&1 | tee $ERROR_OUT/error-line-$iteration.log
    fi
  fi
}

function main () {
  echo "url,new_url,line" > $OUTPUT

  while IFS="," read -r path new_path
  do
    if [ $iteration -ne 1 ];
    then
      echo "line: $iteration"
      echo "path: $path"
      echo "new_path: $new_path"
      echo "Checking if $PROTOCOL://$SITE_URL${path} redirects to $PROTOCOL://$SITE_URL${new_path}"
      if [ $AUTH -eq 0 ];
      then
        check_noauth
      else
        check_auth
      fi
      echo ""
      sleep $((1 + RANDOM % DELAY))s
    fi
    iteration=$iteration+1
  done
}

check_variables
main
