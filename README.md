# Redirect Checker

Takes a CSV file directed in and reads the first two columns to get a list of hopeful redirect paths to check, then checks each using wget.


Example: `SITE_URL=site.example.com ./check-redirect.sh < redirections.csv`


Where redirections.csv contains 
```
path,new_path,... (you can have as many other columns, this script only cares about the first two)
/path,/new-path,...
/other-path,/other-new-path,...
/other-example/old,/other-ex/new-path,...
```

This script will wget `path`, checking if it redirects to `new_path`.

If `CHECK` is set, on failures the output of wget will be logged to `ERROR_OUT/error-line-(line number of original csv file).log`


## All possible environment variables:

Set the protocol to http or https (required)

`PROTOCOL=http`

Set the site url, do not set https or use a trailing slash (required)

`SITE_URL=site.example.com`

Set the http basic auth username and password (required to set both or neither)

`USER=username`

`PASS=password`

Set the output csv file

`OUTPUT=broken.csv`

Set the base delay, in seconds, which 1 to 3 seconds will be added onto

`DELAY=1`

Set if the output of the wget command should be logged

`CHECK=true`

Set the directory into which the output of the wget command should be logged

`ERROR_OUT=errors`

---
Copyright 2022 Madeline Holland
MIT License
